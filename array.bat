#include <iostream>
using namespace std;

void printarr(int* mas, int size) {
	cout << endl;
	for (int i = 0; i < size; i++) {
		cout << "arr[" << i << "]=" << mas[i] << endl;
	}
}

	int main()
	{
		int massive[100];
		int size;
		cin >> size;
		for (int z = 0; z < size; z++) {
			std::cin >> massive[z];
		}
		for (int c = 0; c < size; c++) {
			for (int k = c; k < size; k++) {
				if (massive[c] > massive[k]) {
					int g = massive[c];
					massive[c] = massive[k];
					massive[k] = g;
				}
			}
		}
		printarr(massive, size);
		return 0;
	}