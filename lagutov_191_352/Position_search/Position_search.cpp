﻿
//Example 1:
//Input: [1, 3, 5, 6] , 5
//Output : 2
//Example 2 :
//	Input : [1, 3, 5, 6] , 2
//	Output : 1

#include <iostream>

using namespace std;

int find(int arr[], int f, int num) {

	for (int i = 0; i < f; i++)
	{
		if (arr[i] == num)
		{
			return i;
		}
		else if (num > arr[i - 1] && num < arr[i]) {
			return i++;
		}
	}
}

int main()
{
	int f;
	cin >> f;
	int* mas = new int[f];
	for (int i = 0; i < f; i++) {
		cin >> mas[i];
	}
	int number;
	cin >> number;
	cout << find(mas, f, number);
}