#pragma once

#include "DBRecord.h"
#include <vector>
#include <iterator>

class DBClass
{
public:

	vector<DBRecord> basedan; //������, ���������� �������� �������� ������ �� ���������� �����

public:

	DBClass();
	~DBClass();

	int load(const string& filename, const char* key);

	void printAll();

	int find(const string& secondname);

	int add(const char* nameNEW, const char* second_nameNEW, const char* passportNEW);

	int remove(const int n);

	void save(const string& filename, const char* key);

	void sort(bool is);


};
