
#include "DBRecord.h"
#include "DBClass.h"

int main()
{
	DBClass testDB;
	testDB.load("document.txt", "1234");
	testDB.printAll();

	testDB.basedan[3].print();

	cout << testDB.find("Mcdonald") << endl;

	cout << testDB.add("Ivan", "Ivanov", "1234 567890") << endl;

	cout << testDB.remove(2) << endl;

	testDB.printAll();

	testDB.sort(true);
	
	testDB.printAll();

	testDB.save("copyBD.txt", "1234");
}
