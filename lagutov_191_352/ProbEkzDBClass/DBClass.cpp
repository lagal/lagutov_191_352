
#include "DBClass.h"

DBClass::DBClass()
{
}
DBClass::~DBClass()
{
}


int DBClass::load(const string& filename, const char* key)
{
	ifstream file; // ������� ������ ������ ifstream
	file.open(filename, fstream::in); // ��������� ����
	
	string tempstr;

	if (!file.is_open())
		return -1;
	
	else {
		while (!file.eof())
		{
			getline(file, tempstr);
			if (tempstr[0] == '{') {
				file.get();
				DBRecord people;
				getline(file, tempstr);
				size_t ravno = tempstr.find('=');
				size_t kav = tempstr.rfind('\"');
				people.name = tempstr.substr(ravno + 2, kav - ravno - 2);
				file.get();
				getline(file, tempstr);
				ravno = tempstr.find('=');
				kav = tempstr.rfind('\"');
				people.second_name = tempstr.substr(ravno + 2, kav - ravno - 2);
				file.get();
				file.get();
				getline(file, tempstr);
				ravno = tempstr.find('=');
				kav = tempstr.rfind('\"');
				people.passport = tempstr.substr(ravno + 2, kav - ravno - 2);
				file.get();
				basedan.push_back(people);
				
			}
			
		}
		return 0;
	}
}

void DBClass::printAll()
{
	cout << "--------------------------" << endl;
	for (unsigned int i = 0; i < basedan.size(); i++) {
	
		cout << endl;
		cout << "FirstName: " << basedan[i].name << endl;
		cout << "Lastname: " << basedan[i].second_name << endl;
		cout << "Passport: " << basedan[i].passport << endl;
		cout << endl;
		
	}
	cout << "--------------------------" << endl;
}

int DBClass::find(const
	string& secondname) {
	for (unsigned int i = 0; i < basedan.size(); i++) {
		if (basedan[i].second_name == secondname) {
			return i;
		}
	}
	return -1;
}

int DBClass::add(const
	char* nameNEW, const char*
	second_nameNEW, const char
	* passportNEW) {

	//������� ������
	//���-(-1)
	size_t nameSize = strlen(nameNEW);
	size_t secnameSize = strlen(second_nameNEW);
	size_t passSize = strlen(passportNEW);

	if (passSize != 11) {
		return -1;
	}

	if (passportNEW[4] != ' ') {
		return -1;
	}

	for (unsigned int i = 0; i < 4; i++) {
		if (passportNEW[i] >= '0' && passportNEW[i] <= '9') {}
		else {
			return -1;
		}
	}
	for (unsigned int i = 5; i < 10; i++) {
		if (passportNEW[i] >= '0' && passportNEW[i] <= '9') {}
		else {
			return -1;
		}
	}
	
	if (nameNEW[0] >= 'A' && nameNEW[0] <= 'Z') {

	}
	else {
		return -1;
	}
	for (unsigned int i = 1; i < nameSize; i++) {
		if (nameNEW[i] >= 'a' && nameNEW[i] <= 'z') {

		}
		else {
			return -1;
		}
	}
	if (second_nameNEW[0] >= 'A' && second_nameNEW[0] <= 'Z') {

	}
	else {
		return -1;
	}
	for (unsigned int i = 1; i < secnameSize; i++) {
		if (second_nameNEW[i] >= 'a' && second_nameNEW[i] <= 'z') {

		}
		else {
			return -1;
		}
	}
	DBRecord temp;
	temp.name = nameNEW;
	temp.second_name = second_nameNEW;
	temp.passport = passportNEW;
	basedan.push_back(temp);
	return basedan.size();
}

int DBClass::remove(const
	int n) {
	if (n<0 || n>basedan.size()) {
		return -1;
	}
	else {
		basedan.erase(basedan.begin() + n);
		return 0;
	}
}

void DBClass::save(const
		string& filename, const
		char* key) {

	ofstream copyfile(filename);

	for (int i = 0; i < basedan.size(); i++) {
		copyfile << '{' << endl;
		copyfile << '\t' << "\"Name\"=\"" << basedan[i].name << "\"," << endl;
		copyfile << '\t' << "\"Second name\"=\"" << basedan[i].second_name << "\"," << endl;
		copyfile << '\t' << "\"Passport\"=\"" << basedan[i].passport << "\"," << endl;
		copyfile << '}' << endl;
	}
}

void DBClass::sort(bool
	is) {
	if (is == true) {
		for (unsigned int i = 0; i < basedan.size(); i++) {
			for (unsigned int j = 0; j < basedan.size(); j++)
				if (basedan[i].second_name[0] < basedan[j].second_name[0]) {
					std::swap(basedan[j], basedan[i]);
			}
		}
	}
	else {
		for (unsigned int i = 0; i < basedan.size(); i++) {
			for (unsigned int j = 0; j < basedan.size(); j++)
				if (basedan[i].second_name[0] > basedan[j].second_name[0]) {
					std::swap(basedan[j], basedan[i]);
				}
		}
	}
}