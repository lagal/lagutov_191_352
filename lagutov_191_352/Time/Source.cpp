
// ������ 7
/*
��������� ������� ��������� �� ������� ���������� ��������
��� ����, �����, �������, ������, ���������, ������� �� n ���������.
������ ��������:
���������� � ������
���������� � �����
���������� � ��������
�������� �� ������
�������� � �����
�������� �� ��������
�������� �������� �� ������
�������� �������� � �����
�������� �������� �� ��������
*/

#include <iostream>
#include <deque>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <iterator>

using namespace std;

void print(deque <int> deq,
	stack <int> stk,
	queue <int> que,
	list <int> lst,
	set <int> st,
	int mas[],int n) {
	cout << "Deq:" << endl;
	for (auto n : deq) {
		cout << n << " ";
	}
	cout << endl;

	cout << "List:" << endl;
	for (auto n : lst) {
		cout << n << " ";
	}
	cout << endl;

	cout << "Set:" << endl;
	copy(st.begin(), st.end(), ostream_iterator<char>(cout, " "));
	cout << endl;

	cout << "Mas:" << endl;
	for (int i = 0; i < n; i++) {
		cout << mas[i] << " ";
	}
	cout << endl;
}


int main() {

	deque <int> deq{ 1,2,3,4,5 };
	stack <int> stk(deq);
	queue <int> que(deq);
	list <int> lst{ 1,2,3,4,5 };
	set <int> st{ 1,2,3,4,5 };
	int mas[] = { 1,2,3,4,5 };

	//���������� � ������

	deq.push_front(10);
	stk.push(10);
	que.push(10);
	lst.push_front(10);

	//���������� � �����

	deq.push_back(10);
	lst.push_back(10);


	//���������� � ��������

	deque <int> ::iterator d = deq.begin() + (deq.size() / 2);

	deq.insert(d, 10);


	//�������� �� ������

	deq.pop_front();
	que.pop();
	lst.pop_front();


	//�������� � �����

	deq.pop_back();
	stk.pop();
	lst.pop_back();

	//�������� �� ��������


	//�������� �������� �� ������

	deq.front();
	que.front();
	lst.front();

	//�������� �������� � �����

	deq.back();
	stk.top();
	lst.back();

	//�������� �������� �� ��������

	cout << mas[5 / 2];


}