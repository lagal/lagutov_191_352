﻿
#include <iostream>
using namespace std;

void pascalstring(int n)
{
    int i, j;
    int* st = new int[n];
    for (i = 1; i < n; i++) {
        st[i] = 0;
        }
    st[0] = 1;
    for (j = 1; j < n; j++)
        for (i = j; i >= 1; i--)
            st[i] += st[i - 1];
    for (i = 0; i < n; i++)
        cout << st[i] << " ";

}

int main()
{
    int numofstring;
    cin >> numofstring;
    pascalstring(numofstring);

}

