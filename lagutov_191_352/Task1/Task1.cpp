
#include <iostream>
#include <stack>
#include <queue>
#include <string>



using namespace std;

void check(stack <char> mystack)
{
    
    string text;
    cin >> text;

    bool res = true;
    for (int i = 0; i < text.length(); i++) {
        
        for (int i = 0; i < text.length(); i++)
        {
            switch (text[i])
            {
            case '(':
                mystack.push('(');
                break;
            case '{':
                mystack.push('{');
                break;
            case '[':
                mystack.push('[');
                break;
            case ')':
                if (!mystack.empty() && mystack.top() == '(')
                    mystack.pop();
                else res = false;
                break;
            case '}':
                if (!mystack.empty() && mystack.top() == '{')
                    mystack.pop();
                else res = false;
                break;
            case ']':
                if (!mystack.empty() && mystack.top() == '[')
                    mystack.pop();
                else res = false;
                break;
            }
        }
    }
   
    
    if (mystack.empty() && res == true) cout << "True";
    else cout << "False";
}

int main()
{
    stack <char> mystack;
    check(mystack);

}
