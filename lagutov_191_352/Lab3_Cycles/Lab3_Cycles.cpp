﻿
#include <iostream>
#include <cmath>

using namespace std;

void evenSum(int num) {

	int digit = 0, sum = 0, even = 0;

	while (num != 0) {
		digit = num % 10;
		sum += digit;
		if (digit % 2 == 0) {
			even += digit;
		}
		num /= 10;
	}
	if (even == 0) {
		cout << "No even digits" "\n";
	}
	else if (even < 0) {
		cout << -even << endl;
	}
	else {
		cout << even << endl;
	}

}

void oddSum(int num) {

	int digit = 0, sum = 0, odd = 0;

	while (num != 0) {
		digit = num % 10;
		sum += digit;
		if (digit % 2 != 0) {
			odd += digit;
		}
		num /= 10;
	}
	if (odd == 0) {
		cout << "No odd digits" "\n";
	}
	else if (odd < 0) {
		cout << -odd << endl;
	}
	else {
		cout << odd << endl;
	}

}

void sumofDigit(int num) {

	int digit = 0, sum = 0;

	while (num != 0) {
		digit = num % 10;
		sum += digit;
		num /= 10;
	}
	if (sum < 0) {
		cout << -sum << endl;
	}
	else {
		cout << sum << endl;
	}

}

bool polindrom(int num) {

	int digit = 0, digit_1 = 0, digit_k = 0, numX = 0, amountOfDigit = 0;

	numX = num;
	while (num != 0) {
		digit = num % 10;
		amountOfDigit++;
		num /= 10;
	}
	while (amountOfDigit >= 1) {
		digit_1 = numX % 10;
		digit_k = numX / pow(10, (amountOfDigit - 1));
		if (digit_1 == digit_k) {
			numX -= digit_k * pow(10, (amountOfDigit - 1));
			numX /= 10;
			amountOfDigit -= 2;
		}
		else {
			break;
		}
	}
	if (digit_1 == digit_k) {
		return true;
	}
	else {
		return false;
	}
}

int main()
{
	int num;
	cout << "num = ";
	cin >> num;

	cout << "1.Even \n"
		"2.Odd \n"
		"3.Sum of digits \n"
		"4.Polindrom check" << endl;

	int prog;

	cin >> prog;

	switch (prog) {

	case (1): {
		evenSum(num);
		break;
	}

	case (2): {
		oddSum(num);
		break;
	}

	case (3): {
		sumofDigit(num);
		break;
	}

	case (4): {
		if (polindrom(num) == true) {
			cout << "Polindrom" << endl;
		}
		else {
			cout << "Ne polindrom" << endl;
		}
		break;
	}
	}

}
