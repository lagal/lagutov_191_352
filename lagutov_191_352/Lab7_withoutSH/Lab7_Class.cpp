﻿
#include <iostream>
#include "matrix.h"

using namespace std;


void task1(){

	matrix matr;

	matr.input();
	int number;
	cin >> number;
	matr.matrixmultnum(number).print();

}

void task2() {

	matrix matr, matr2;

	matr.input();
	matr2.input();

	
	matr.matrixmult(matr2).print();

}

void task3() {
	matrix matr;
	int num;

	matr.input();
	cin >> num;


	matr.matrixmultnum(num).print();
}

void task4() {
	matrix matr;

	matr.input();
	matr.matrixtrace(matr).print();
}

int main()
{
	cout << "What? \n"
		"1) Sum \n"
		"2) MultMat \n"
		"3) MultNum \n"
		"4) Trace \n"
		"5) Exit \n";

	int prog;
	cin >> prog;

	switch (prog) {

	case(1): {

		task1();
		break;

	}

	case(2): {

		task2();
		break;

	}

	case(3): {

		task3();
		break;
	}

	case(4): {

		task4();
		break;
	}

	case(5): {
		break;
	}
	}

	
	matrix m_1, m_2;
	int a;

	cin >> m_1;
	cin >> m_2;
	cin >> a;

	cout << "-------------------------" << endl;

	matrix sum;
	cin >> sum;
	sum-= m_1 + m_2;
	cout << sum;
}
