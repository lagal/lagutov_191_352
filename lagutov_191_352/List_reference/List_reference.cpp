﻿//Создать list
//front, back
//empty, size
//clear
//insert
//erase
//push_back, pop_back
//push_front, pop_front
//swap
//merge
//remove, remove_if
//unique
//sort
//уничтожить список



#include <list>
#include <iostream>
#include <iterator>

using namespace std;

void printlist(list <int> mas){
	copy(mas.begin(), mas.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
}

int main()
{

	list <int> arr{ 1, 2, 3, 4, 5 };
	list <int> arr2{ 6, 5, 4, 3, 2, 1 };
	

	cout << "Begin : " << arr.front() << endl;
	cout << "End : " << arr.back() << endl;

	if (arr.empty()) {
		cout << "Empty" << endl;
	}
	else {
		cout << "Not empty" << endl;
	}

	cout << "Size of list = " << arr.size() << endl;

	cout << "Input 3 at begin : " << endl;
	    arr.push_front(3);
		printlist(arr);

	cout << "Input 5 at end : " << endl;
		arr.push_back(5);
		printlist(arr);
		
	list<int>::iterator it = arr.begin();
	advance(it, 2);// итератор на позиции 2

	cout << "Input 55 after pos 2 : " << endl;
	arr.insert(it, 55);
	printlist(arr);

	cout << "Delete 55 : " << endl;
	
	arr.erase(prev(it, 1));
	printlist(arr);

	cout << "Delete last & first el. : " << endl;
	arr.pop_back();
	arr.pop_front();
	printlist(arr);


	arr.swap(arr2);
	cout << "Swar list_1 and list_2 :  \n" 
		<< "list_1 : " << endl;
	printlist(arr);
	cout << "list_2 :  \n";
	printlist(arr2);

	cout << "Sort list_2 : " << endl;
	arr2.sort();
	printlist(arr2);
	cout << "Sort list_1(great) : " << endl;
	arr.sort(greater<int>());
	printlist(arr);

	arr.sort();
	cout << "Merge list_1 with list_2 : " << endl;
	arr.merge(arr2);
	printlist(arr);

	cout << "Unique merged list : " << endl;
	arr.unique();
	printlist(arr);

	cout << "Delete 4 (with .remove) : " << endl;
	arr.remove(4);
	printlist(arr);

	cout << "Delete all <5 : " << endl;
	arr.remove_if([](int n) { return n < 5; });
	printlist(arr);


	arr.clear();
	cout << "List cleared " << endl;
	printlist(arr);

}