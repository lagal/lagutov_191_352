#pragma once
#include "matrix.h"
class dsquare :
	public matrix
{

public:
	dsquare();
	~dsquare();
	dsquare(int t_rows, int t_columns);
	dsquare(int t_rows, int t_columns, int* mas);
	void print();
	void input();
};