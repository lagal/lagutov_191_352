
#include "Vector.h"
#include <iostream>
#include "dsquare.h"


dsquare::dsquare()
{
}

dsquare::~dsquare()
{
}

dsquare::dsquare(int t_rows, int t_col)
{
	columns = t_col;
	rows = t_rows;
	arr = new int[rows * columns];
}

dsquare::dsquare(int t_rows, int t_col, int* mas)
{
	rows = t_rows;
	columns = t_col;
	arr = new int[rows * columns];
	for (int i = 0; i < rows * columns; i++) {
		arr[i] = mas[i];
	}

}

void dsquare::input()
{
	cout << "Input size of matrix: ";
	cin >> rows;
	columns = rows;
	arr = new int[rows * columns];

	for (int i = 0; i < (columns * rows); i++) {
		arr[i] = 0;
	}

	for (int j = 0; j < columns; j++) {
		cin >> arr[j + columns * j];
	}
};

void dsquare::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < rows; j++)
		{
			cout << arr[i * rows + j] << "\t";
		}
		cout << endl;
	}
}