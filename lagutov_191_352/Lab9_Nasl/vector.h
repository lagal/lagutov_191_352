#pragma once
#include "Matrix.h"
class vector :
	public matrix
{

public:
	vector();
	~vector();
	vector(int t_columns);
	vector(int t_columns, int* mas);
	void mult(const vector& vec2);
	void input();
	void length();
	void print();
};

