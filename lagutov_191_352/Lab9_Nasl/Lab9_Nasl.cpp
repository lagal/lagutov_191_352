﻿#include <iostream>
#include "matrix.h"
#include "vector.h"
#include "dsquare.h"

using namespace std;

int main() {
	vector mat1, mat2;
	mat1.input();
	mat2.input();
	mat1.mult(mat2);
}