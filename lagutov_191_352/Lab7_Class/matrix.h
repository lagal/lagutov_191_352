#pragma once


#include <iostream>
using namespace std;

template <class type>
void printMatrix(type* matr, int matr_col, int matr_rows);


template <typename type>
class matrix
{

private:

	int rows;
	int columns;
	type* arr;

public:

	matrix();
	~matrix();

	matrix(int t_rows, int t_columns);
	matrix(int t_rows, int t_columns, type* mas);

	//sum
	matrix<type>& matrixsum(const matrix<type>& mat2);

	//mult
	matrix<type>& matrixmult(const matrix<type>& mat2);

	//multnum
	matrix<type>& matrixmultnum(int number);

	//trace
	matrix<type>& matrixtrace(const matrix<type>& mat);

	
	//det

	//print

	void print();

	//input

	void input();

	matrix<type>& operator - ();

	matrix<type>& operator += (const matrix<type>& mat);

	matrix<type>& operator -= (const matrix<type>& mat);

	//peregruz +
	friend matrix<type>& operator + (const matrix<type>& mat1, const matrix<type>& mat2);

	//peregruz -
	friend matrix<type>& operator - (const matrix<type>& mat1, const matrix<type>& mat2);

	//peregruz *num
	friend matrix<type>& operator * (const matrix<type>& mat1, const int num);

	//peregruz *mat
	friend matrix<type>& operator * (const matrix<type>& mat1, const matrix<type>& mat2);

	//peregruz <<
	friend ostream& operator << (ostream& out, const matrix<type>& mat);

	//peregruz >>
	friend istream& operator >> (istream& in, matrix<type>& mat);
};
template <class type>
inline matrix<type>& operator + (const matrix<type>& mat1, const matrix<type>& mat2) {

	if (mat1.rows == mat2.rows && mat1.columns == mat2.columns) {
		type* res;
		res = new type[mat1.columns * mat1.rows];
		for (int i = 0; i < mat1.rows * mat1.columns; i++) {
			res[i] = mat1.arr[i] + mat2.arr[i];
		}
		matrix temp(mat1.rows, mat1.columns, res);
		return temp;
	}
	matrix temp(0, 0, nullptr);
	return temp;
}
template <class type>
inline matrix<type>& operator - (const matrix<type>& mat1, const matrix<type>& mat2) {

	if (mat1.rows == mat2.rows && mat1.columns == mat2.columns) {
		type* res;
		res = new type[mat1.columns * mat1.rows];
		for (int i = 0; i < mat1.rows * mat1.columns; i++) {
			res[i] = mat1.arr[i] - mat2.arr[i];
		}
		matrix temp(mat1.rows, mat1.columns, res);
		return temp;
	}
	matrix temp(0, 0, nullptr);
	return temp;
}
template <class type>
inline matrix<type>& operator * (const matrix<type>& mat1, const int num) {

		type* res;
		res = new type[mat1.columns * mat1.rows];
		for (int i = 0; i < mat1.rows * mat1.columns; i++) {
			res[i] = mat1.arr[i] * num;
		}
		matrix temp(mat1.rows, mat1.columns, res);
		return temp;
}
template <class type>
inline matrix< type >& operator * (const matrix<type>& mat1, const matrix<type>& mat2) {

	if (mat1.columns == mat2.rows)
	{
		int rescol = mat2.columns;
		int resrows = mat1.rows;
		type* resarr = new type[(mat1.rows) * (mat2.columns)];
		matrix res(resrows, rescol, resarr);
		for (int i = 0; i < (resrows) * (rescol); i++)
			res.arr[i] = 0;

		for (int i = 0; i < resrows; i++)
		{
			for (int j = 0; j < rescol; j++)
			{
				res.arr[i * rescol + j] = 0;
				for (int k = 0; k < rescol; k++)
				{
					res.arr[i * (rescol)+j] += mat1.arr[i * rescol + k] * mat2.arr[k * rescol + j];
				}
			}
		}
		return res;
	}
	else
	{
		std::cout << "Invalid size of matrixes." << std::endl;
		matrix  res(0, 0, nullptr);
		return res;
	}
}

template <class type>
inline ostream& operator << (ostream &out, const matrix<type>&mat) {

	for (int i = 0; i < mat.rows; i++)
	{
		for (int j = 0; j < mat.columns; j++)
		{
			out << mat.arr[i * mat.columns + j] << "\t";
		}
		out << endl;
	}
	return out;
}
template <class type>
inline istream& operator >> (istream& in, matrix<type>& mat) {

	cout << "Input size of matrix: \n"
		"rows = ";
	in >> mat.rows;
	cout << "columns = ";
	in >> mat.columns;
	mat.arr = new type[mat.rows * mat.columns];


	for (int i = 0; i < (mat.columns * mat.rows); i++)
		in >> mat.arr[i];
		return in;
}
