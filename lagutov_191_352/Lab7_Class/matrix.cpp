

#include "matrix.h"
#include <iostream>
using namespace std;

template <class type>
void printMatrix(type* matr, int matr_col, int matr_rows)
{
	for (int i = 0; i < matr_rows; i++)
	{
		for (int j = 0; j < matr_col; j++)
		{
			cout << matr[i * matr_col + j] << "\t";
		}
		cout << endl;
	}
}

template <typename type>
matrix<type> ::matrix()
{
}
template <typename type>
matrix<type>::~matrix()
{
}

template <typename type>
matrix<type>::matrix(int t_rows, int t_columns)
{
	rows = t_rows;
	columns = t_columns;
	arr = new type[rows * columns];
}
template <typename type>
matrix<type>::matrix(int t_rows, int t_columns, type* mas)
{
	rows = t_rows;
	columns = t_columns;
	arr = new type[rows * columns];
	for (int i = 0; i < rows * columns; i++) {
		arr[i] = mas[i];
	}

}
template <typename type>
matrix<type>& matrix<type>::matrixsum(const matrix<type>& mat2)
{
	if (columns == mat2.columns && rows == mat2.rows)
	{
		int resrows = rows;
		int rescolumns = columns;
		matrix  res(resrows, rescolumns);
		//int* mat3 = new int[resrows * rescolumns];
		for (int i = 0; i < resrows * rescolumns; i++)
			//mat3[i,j] = mat1[i, j]+mat2[i,j]
			res.arr[i] = arr[i] + mat2.arr[i];
			//mat3[i] = arr[i] + mat2.arr[i];
		return res;
	}
	else
	{
		matrix  res(0, 0);
		return res;
	}
	
}
template <typename type>
matrix<type>& matrix<type>::matrixmult(const matrix<type>& mat2)
{
	if (columns == mat2.rows)
	{
		int rescol = mat2.columns;
		int resrows = rows;
		type* resarr = new type[(rows) * (mat2.columns)];
		matrix res(resrows, rescol, resarr);
		for (int i = 0; i < (rows) * (columns); i++)
			res.arr[i] = 0;

		for (int i = 0; i < resrows; i++)
		{
			for (int j = 0; j < rescol; j++)
			{
				res.arr[i * rescol + j] = 0;
				for (int k = 0; k < rescol; k++)
				{
					res.arr[i * (rescol)+j] += arr[i * rescol + k] * mat2.arr[k * rescol + j];
				}
			}
		}
		return res;
	}
	else
	{
		std::cout << "Invalid size of matrixes." << std::endl;
		matrix res(0, 0, nullptr);
		return res;
	}
}
template <typename type>
matrix<type>& matrix<type>::matrixmultnum(int number)
{
	int resrows = rows;
	int rescolumns = columns;
	matrix res(resrows, rescolumns);
	for (int i = 0; i < resrows * rescolumns; i++)
		res.arr[i] = arr[i] * number;
	return res;
	
}
template <typename type>
matrix<type>& matrix<type>::matrixtrace(const matrix<type>& mat)
{
	int sum = 0;
	int p = 0;

	if (mat.rows == mat.columns) {

		for (int i = 0; i < mat.columns; i++) {

			p = mat.arr[i + mat.columns * i];
			sum += p;
			p = 0;
			
		}

	}

	else
	{
		cout << "Error";
	}
	
	matrix res(1, 1);
	res.arr[1] = sum;
	return res;

}

template <class type>
void matrix<type>::print()
{
	printMatrix(arr, columns, rows);
}
template <class type>
void matrix<type>::input()
{
	cout << "Input size of matrix: \nrows = ";
	cin >> rows;
	cout << "columns = ";
	cin	>> columns;
	arr = new type[rows * columns];
	

	for (int i = 0; i < (columns * rows); i++)
		cin>> arr[i];
}

template<class type>
matrix<type>& matrix<type>::operator-()
{
	for (int i = 0; i < columns * rows; i++) {
		arr[i] = -arr[i];
	}
	return *this;
}

template <class type>
matrix<type>& matrix<type>::operator+=(const matrix<type>& mat)
{
	if (rows = mat.rows && columns == mat.columns) {
		for (int i = 0; i < rows * columns; i++) {
			arr[i] += mat.arr[i];

		}
		return *this;
	}
}
template <class type>
matrix<type>& matrix<type>::operator-=(const matrix<type>& mat)
{
	if (rows = mat.rows && columns == mat.columns) {
		for (int i = 0; i < rows * columns; i++) {
			arr[i] -= mat.arr[i];

		}
		return *this;
	}
}
