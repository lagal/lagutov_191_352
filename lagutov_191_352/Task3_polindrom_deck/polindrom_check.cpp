
//������ 3
/*
���������, �������� �� ��������� ������ �����������
(�������� ��������� ����� ������� � ������ ������). ������������ ���.
*/


#include <iostream>
#include <deque>

using namespace std;

bool polindrom_check(string text) {

	deque<int> check;
	for (int i = 0; i < text.length(); i++) {
		check.push_back(text[i]);
	}

	for (int i = 0; i <= (check.size()) / 2; i++) {
		if (check.front() == check.back()) {
			check.pop_back();
			check.pop_front();
		}
		else {
			return false;
		}
	}

	return true;

}

int main() {

	string text;
	cin >> text;
	bool ch = polindrom_check(text);
	if (ch == true) {
		cout << "Polindrom";
	}
	else {
		cout << "Ne polinrom";
	}
}