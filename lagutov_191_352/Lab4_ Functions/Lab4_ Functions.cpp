﻿
// Написать функцию печатающую квадраты всех натуральных чисел от 0 до заданного n. 
//Разрешается использовать из арифметических операций лишь сложение и вычитание. 
//Общее число операций должно быть порядка n.
//Указание: воспользоваться(n + 1) ^ 2 = n ^ 2 + 2 * n + 1

#include <iostream>

using namespace std; 


void square(unsigned int n) {

	int p = n;
	
	for (int f = n; f >= 0; f--) {

		cout << n << "^2=";

		for (int i = n; i > 1; i--) {
			n += p;
		}

		cout << n << endl;
		n = f-1;
		p = f-1;
	}

}


int main() {

	unsigned int n;
	
	cin >> n;

	square(n);

}



