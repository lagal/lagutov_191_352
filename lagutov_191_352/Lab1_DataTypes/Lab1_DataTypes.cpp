﻿
#include <iostream>
using namespace std;

int main()
{
	cout << "My name Sasha. I'm 18 years old \n";
	int num1, num2;
	double x;
	cout << "num1 = ";
	cin >> num1;
	cout << "num2 = ";
	cin >> num2;

	if (num1 == 0 && num2 == 0)
		cout << "x - any value";
	else if (num1 == 0) 
		cout << "x - empty set";
	else {
		x = ((double)-num2) / num1;
		cout << num1 << "* x + " << num2 << " = 0" << endl;
		cout << num1 << "* x = " << -num2 << endl;
		cout << "x = " << -num2 << "/" << num1 << endl;
		cout << "x = " << x << endl;
		cout << "result: " << x << endl;
	}
	
}

/*
Input 2 numbers: 1 3

1*х + 3 = 0
1*х = -3
х = -3/1
х = -3
Result: -3.

*/