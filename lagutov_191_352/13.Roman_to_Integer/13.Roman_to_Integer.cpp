﻿
#include <iostream>
using namespace std;

int value(char roman)
{

	switch (roman)
	{
	case 'I':return 1;
	case 'V':return 5;
	case 'X':return 10;
	case 'L':return 50;
	case 'C':return 100;
	case 'D':return 500;
	case 'M':return 1000;
	default: {
		cout << "Error" << endl;
		exit(0);
		break;
	}
	}

}

int romanToInt(string roman)
{

	int sum = 0, valueNext = 0;

	for (int i = roman.length() - 1; i >= 0; i--)
	{
		if (value(roman[i]) >= valueNext)
			sum += value(roman[i]);
		else
			sum -= value(roman[i]);

		valueNext = value(roman[i]);
	}
	return sum;

}

int main() {

	string s;
	cin >> s;
	cout << romanToInt(s);

}